describe('pairingController', function() {

    /* App init and config before every test */ 
    var $scope;
    beforeEach(module('pairing'));

    beforeEach(inject(function($rootScope, $controller) {
        $scope = $rootScope.$new();
        $controller('pairingController', {$scope: $scope});
    }));

    /* Check if all variables are (well) defined */
    it('should have private variables defined', function() {
        expect($scope.formdata).toEqual({});
        expect($scope.developers).toBeDefined();
        expect($scope.developersList).toBeDefined();
        expect($scope.pairingResultsReady).toBe(false);
        expect($scope.developerListReady).toBe(false);
        expect($scope.generateResultsReady).toBe(false);
        expect($scope.developerError).toBe(false);
        expect($scope.developerNumber).toBe(0);
        expect($scope.developerPairingNumber).toBe(0);
    });

    it('should add a (valid) developer to developers array', function() {
        /* This simulates the input data flow */
        $scope.formdata.name = 'Seb';
        $scope.addDeveloper();

        expect($scope.developers.length).toBe(1);
        expect($scope.developerListReady).toBe(true);
        expect($scope.developers).toEqual(['Seb']);

    });

    it('should not add a duplicate developer to developers array', function() {
        $scope.formdata.name = 'Seb';
        $scope.addDeveloper();
        $scope.formdata.name = 'Seb';
        $scope.addDeveloper();
        
        /* Variable that controls error message */
        expect($scope.developerError).toBe(true);
        expect($scope.developers).toEqual(['Seb']);
    });

    it('should remove a developer from the developer list', function() {
        $scope.formdata.name = 'Seb';
        $scope.addDeveloper();
        $scope.formdata.name = 'Francesco';
        $scope.addDeveloper();

        $scope.removeDeveloper(0);

        var expectedArray = ['Francesco'];

        expect($scope.developers).toEqual(expectedArray);        
    });

    /* Pairing list test */
    it('should generate a valid combination of developer pairing - 4 Developers', function() {
        $scope.developers = ['Seb', 'Richard', 'Francesco', 'Mateusz'];
        $scope.generateList();

        var expectedArray = ['Seb - Richard', 
                             'Seb - Francesco', 
                             'Seb - Mateusz', 
                             'Richard - Francesco', 
                             'Richard - Mateusz', 
                             'Francesco - Mateusz'];

        expect($scope.pairingResultsReady).toBe(true);
        expect($scope.developersList).toEqual(expectedArray);
    });

    it('should generate an empty array of developer pairing - 0 Developers', function() {
        $scope.developers = [];
        $scope.generateList();

        var expectedArray = [];

        expect($scope.developersList).toEqual(expectedArray);
    });

    it('should generate an empty array of developer pairing - 1 Developers', function() {
        $scope.developers = ['Seb'];
        $scope.generateList();

        var expectedArray = [];

        expect($scope.developersList).toEqual(expectedArray);
    });

    it('should generate a valid combination of developer pairing - 2 Developers', function() {
        $scope.developers = ['Seb', 'Richard'];
        $scope.generateList();

        var expectedArray = ['Seb - Richard'];

        expect($scope.developersList).toEqual(expectedArray);
        
    });

});