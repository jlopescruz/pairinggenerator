describe('developersService', function() {

    beforeEach(module('pairing'));
    beforeEach(inject(function($injector) {
        developersService = $injector.get('developersService');
    }));

    /* Service test */
    it('should be defined', inject(function(developersService) {
        expect(developersService).toBeDefined();
    }));

    /* Service vars test */
    it('should have developers array (well) defined', inject(function(developersService) {
         expect(developersService.developers).toEqual([]);
         expect(developersService.developers).toBeDefined();
    }));

    it('should have developers pair list array (well) defined', inject(function(developersService) {
         expect(developersService.developersPairList).toEqual([]);
         expect(developersService.developersPairList).toBeDefined();
    }));

    /* GetDevelopers function test */
    it('should return a valid array of developers', inject(function(developersService) {
        developersService.developers = ['Seb', 'Richard', 'Francesco', 'Mateusz'];
        var expectedArray = ['Seb', 'Richard', 'Francesco', 'Mateusz'];
        
        expect(Object.prototype.toString.call(developersService.getDevelopers())).toEqual('[object Array]');
        expect(developersService.getDevelopers()).toEqual(expectedArray);
    }));

    /* Add Developer function test */
    it('should return false when developer name is not a string', inject(function(developersService) {
        expect(developersService.addDeveloper()).toBe(false);
        expect(developersService.addDeveloper(0)).toBe(false);
        expect(developersService.addDeveloper([])).toBe(false);
        expect(developersService.addDeveloper({})).toBe(false);
        expect(developersService.addDeveloper(true)).toBe(false);
    }));

    it('should return false when trying to add a duplicate developer name', inject(function(developersService) {
        developersService.developers = ['Seb', 'Richard', 'Francesco', 'Mateusz'];
        expect(developersService.addDeveloper('Seb')).toBe(false);
    }));

    it('should return true when a developer is added', inject(function(developersService) {
        var returnedValue = developersService.addDeveloper('Seb');
        expect(returnedValue).toBe(true);
    }));

    it('should add a developer to developer list if name is not duplicate and a string', inject(function(developersService) {
        developersService.addDeveloper('Seb');
        expect(developersService.developers).toEqual(['Seb']);
    }));

    /* Remove Developer function test */
    it('should return false when index parameter is not a number', inject(function(developersService) {
        developersService.developers = ['Seb', 'Richard', 'Francesco', 'Mateusz'];

        expect(developersService.removeDeveloper()).toBe(false);
        expect(developersService.removeDeveloper('')).toBe(false);
        expect(developersService.removeDeveloper([])).toBe(false);
        expect(developersService.removeDeveloper({})).toBe(false);
        expect(developersService.removeDeveloper(true)).toBe(false);
    }));

    it('should return true when a developer is removed', inject(function(developersService) {
        developersService.developers = ['Seb', 'Richard', 'Francesco', 'Mateusz'];
        
        var returnedValue = developersService.removeDeveloper(0);
        expect(returnedValue).toBe(true);
    }));

    it('should remove the developer from the list', inject(function(developersService) {
        developersService.developers = ['Seb', 'Richard', 'Francesco', 'Mateusz'];
        var expectedArray = ['Richard', 'Francesco', 'Mateusz'];
        developersService.removeDeveloper(0);
        expect(developersService.developers).toEqual(expectedArray);
    }));

    /* Set developers pair list function test */
    it('should return false when array parameter is not a number', inject(function(developersService) {
        expect(developersService.setDevelopersPairList()).toBe(false);
        expect(developersService.setDevelopersPairList('')).toBe(false);
        expect(developersService.setDevelopersPairList(1)).toBe(false);
        expect(developersService.setDevelopersPairList({})).toBe(false);
        expect(developersService.setDevelopersPairList(true)).toBe(false);
    }));

    it('should return true when the pair list is set', inject(function(developersService) {
        expect(developersService.setDevelopersPairList(['Seb', 'Richard', 'Francesco', 'Mateusz'])).toBe(true);
    }));

    it('should update the developers pairing list', inject(function(developersService) {
        developersService.setDevelopersPairList(['Seb', 'Richard', 'Francesco', 'Mateusz']);
        expect(developersService.developersPairList).toEqual(['Seb', 'Richard', 'Francesco', 'Mateusz']);
    }));

    /* Get Developers pair list function test */
    it('should return a valid array', inject(function(developersService) {
        developersService.setDevelopersPairList(['Seb', 'Richard', 'Francesco', 'Mateusz']);
        expect(developersService.getDevelopersPairList()).toEqual(['Seb', 'Richard', 'Francesco', 'Mateusz']);
    }));
});