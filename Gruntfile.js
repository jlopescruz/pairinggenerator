module.exports = function (grunt) {

    'use strict';

    /* constants */
    var SRC         = './src/';
    var DIST        = './dist/';

    /* GRUNT INIT =-==-=-=-=-=-=-=-=-=-=-=- */
    grunt.initConfig({
        // load package file
        pkg: grunt.file.readJSON('package.json'),

        // automatically add prefixes
        autoprefixer: {
            options: {
                browsers: ['last 2 versions', '> 1%', 'ios 4', 'ff 17', 'ie 8', 'ie 7']
            },
            all: {
                files: [{
                    expand: true,
                    src: SRC + 'css/main.min.css',
                    cwd: SRC,
                    dest: SRC + 'css/main.min.css',
                    ext: '.css'
                }]
            }
        },

        clean: {
            all: [DIST]
        },

        copy: {
            options: {
                processContentExclude: ['.DS_Store', '.gitignore', '.sass-cache', 'node_modules']
            },
            images: {
                files: [
                    {
                        cwd: SRC,
                        dest: DIST,
                        src: ['images/*.{gif,jpg,png}'],
                        expand: true,
                        filter: 'isFile'
                    }
                ]
            },
            styles: {
                files: [
                    {
                        cwd: SRC,
                        dest: DIST,
                        src: ['css/**'],
                        expand: true,
                        filter: 'isFile'
                    }
                ]
            },
            html: {
                files: [
                    {
                        cwd: SRC + '/html/',
                        dest: DIST,
                        src: ['index.html'],
                        expand: true,
                        filter: 'isFile'
                    }
                ]
            }
        },

        jshint: {
            options: {
                browser: true,
                curly: true,
                devel: true,
                eqeqeq: true,
                evil: true,
                immed: true,
                regexdash: true,
                asi : true,
                sub: true,
                trailing: true,
                globals: {
                    jQuery: true,
                    modernizr: true
                },
                force: true,
                validthis: true
            },
            dev: {
                src: [
                    SRC + 'js/*.js',
                    SRC + 'js/**/*.js',
                    '!' + SRC + 'js/libs/*.js'
                ]
            },
            gruntfile: {
                src: ['Gruntfile.js']
            }
        },

        sass: {
            options: {
                sourcemap: false,
                trace: true
            },
            prod: {
                options: {
                    style: 'compressed'
                },
                files: {
                    './src/css/main.min.css' : './src/sass/main.scss'
                }
            }
        },

        // minify our javascript
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd | hh:MM") %> */\n',
                mangle: false
            },
            build: {
                files: {
                    'dist/js/main.min.js': [
                        SRC + 'js/libs/angular.js',
                        SRC + 'js/app.js',
                        SRC + 'js/controllers/*.js',
                        SRC + 'js/directives/*.js',
                        SRC + 'js/services/*.js',
                        SRC + 'js/partials/*.js'
                    ]
                }
            }
        },

        /*
        * Optimise PNG files.
        * */
        pngmin: {
            build: {
                options: {
                    force: true,
                    ext: '.png'
                },
                files: [
                    {
                        expand: true,
                        src: ['**/*.png'],
                        cwd: DIST + 'images/',
                        dest: DIST + 'images/'
                    }
                ]
            }
        },

        karma: {
            unit: {
                configFile: 'karma.conf.js',
                options: {
                    frameworks: ['jasmine'],
                    files: ['src/js/libs/angular.js',
                            'src/js/libs/angular-mocks.js',
                            'src/js/app.js',
                            'src/js/controllers/*.js',
                            'src/js/services/*.js',
                            'test/controllers/*Spec.js',
                            'test/services/*Spec.js',
                            'test/*Spec.js' ],
                    browsers: ['PhantomJS'],
                    reporters: ['progress', 'coverage'],
                    preprocessors: {
                      'src/js/app.js': ['coverage'],
                      'src/js/controllers/*.js': ['coverage'],
                      'src/js/services/*.js': ['coverage']
                    },

                    // optionally, configure the reporter
                    coverageReporter: {
                        reporters:[
                          {type: 'html', dir:'coverage/'},
                          {type: 'text'}
                        ]
                    },
                    singleRun: true
                }
                
            },
            integration : {
                configFile: 'karma.conf.js',
                options: {
                    autoWatch: true,
                    basePath: '',
                    frameworks: ['ng-scenario'],
                    files: ['test/appIntegration.js'],
                    urlRoot: 'PairingGenerator/dist/',
                    browsers: ['Chrome'],
                    singleRun: false,
                    proxies: {
                        '/': 'http://localhost/'
                    },
                    captureTimeout: 60000
                }
            }
        },

        watch: {
            options: {
                livereload: true
            },
            gruntfile: {
                expand: true,
                files: 'Gruntfile.js',
                tasks: ['jshint:gruntfile', 'clean', 'jshint', 'newer:sass:prod', 'newer:autoprefixer', 'copy']
            },
            sass: {
                expand: true,
                files: [SRC + '**/*.scss'],
                tasks: ['sass:prod', 'autoprefixer', 'copy:styles']
            },
            html : {
                expand: true,
                files: [SRC + 'html/index.html'],
                tasks: ['sass:prod', 'copy:html']
            },
            scripts: {
                expand: true,
                files: [
                    SRC + 'js/**/*.js',
                    SRC + 'js/*.js'
                ],
                tasks: ['clean', 'jshint', 'uglify', 'sass:prod', 'autoprefixer', 'copy']
            }
        }

    });


    /* MODULES =-=-=-=-=-=-=-=-=-=-=-=-=-=- */

    // load every plugin in package.json
    require('matchdep').filter('grunt-*').forEach(grunt.loadNpmTasks);
    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-karma');

    /* TASKS =-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

    grunt.registerTask('run', ['watch']);
    grunt.registerTask('unit', ['karma:unit']);
    grunt.registerTask('integration', ['karma:integration']);
    grunt.registerTask('dev', ['clean', 'jshint', 'uglify', 'sass:prod', 'autoprefixer', 'copy']);
    grunt.registerTask('default', ['dev']);

};