app.controller('pairingController', ['$scope', 'developersService', function($scope, developersService) {

    /* Scope var init */
    $scope.formdata                     = {};
    $scope.developers                   = developersService.getDevelopers();
    $scope.developersList               = [];
    $scope.pairingResultsReady          = false;
    $scope.developerListReady           = false;
    $scope.generateResultsReady         = false;
    $scope.developerError               = false;
    $scope.developerNumber              = 0;
    $scope.developerPairingNumber       = 0;

    /* Minimum developers number for app to work */
    var MIN_DEVELOPERS                  = 3;

    /* Scope function that adds a Developer do the service */
    $scope.addDeveloper = function () {
        $scope.pairingResultsReady = false;
        
        /* Check if input has data */
        if ($scope.formdata.name) {
            /* Check for duplicates */
            if (developersService.addDeveloper($scope.formdata.name) === false ) {
                $scope.developerError = true;
            } else {
                $scope.developerListReady = true;
                getDevelopers();
                isGenerateResultsReady();
            }
        }
    };

    /* Scope function that hides error markup and pairing results list */
    $scope.hideErrorMessage = function () {
        $scope.developerError      = false;
        $scope.pairingResultsReady = false;
    };

    /* Scope function that calls internal getAllDeveloperCombinations function */
    $scope.generateList = function () {
        $scope.pairingResultsReady = true;
        $scope.developersList = getAllDeveloperCombinations();

        developersService.setDevelopersPairList($scope.developersList);
    };

    /* Scope function that removes one developer by index */
    $scope.removeDeveloper = function (index) {
        developersService.removeDeveloper(index);
        $scope.pairingResultsReady = false;
        getDevelopers();
        isGenerateResultsReady();
    };

    /* Private function that gets the Developers from service and updates number of developers scope variable */
    var getDevelopers = function () {
        $scope.developers       = developersService.getDevelopers();
        $scope.developerNumber  = $scope.developers.length;
    };

    /* Private function that checks if generate list button is visible */
    var isGenerateResultsReady = function () {
        $scope.generateResultsReady = ($scope.developerNumber >= MIN_DEVELOPERS)? true : false;
    };

    /* Private function that returns array with all combinations */
    var getAllDeveloperCombinations = function () {

        var allDeveloperCombinations = [], 
            temp = [],
            i;

        /* Recursive function that returns all possible combinations of an array */
        function allCombinations (array, number) {
            var i,
                subI,
                ret = [],
                sub,
                next;

            for (i = 0; i < array.length; i++) {
                if (number === 1) {
                    ret.push( [ array[i] ] );
                } else {
                    sub = allCombinations (array.slice(i+1, array.length), number-1);
                    for (subI = 0; subI < sub.length; subI++ ) {
                        next = sub[subI];
                        next.unshift(array[i]);
                        ret.push(next);
                    }
                }
            }

            return ret;
        }

        /* Array with several array with all combinations. 
           Ex. [[1,2] , [1,3], [2,3]]                       */
        temp = allCombinations($scope.developers, 2);
        $scope.developerPairingNumber = temp.length;

        /* Loops throught temp array and concatenates the 2 results 
           Ex. ['1 - 2' , '1 - 3' ,  '2 - 3']               */
        for (i = 0; i < temp.length; i++) {
            allDeveloperCombinations.push(temp[i][0] + ' - ' + temp[i][1]);
        }

        return allDeveloperCombinations;
    };

}]);