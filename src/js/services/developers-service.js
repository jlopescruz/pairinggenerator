app.factory('developersService', [function() {

    var developersService = {

        developers: [],

        developersPairList: [],

        getDevelopers: function () {
            return developersService.developers;
        },

        addDeveloper: function(name) {
            if (typeof name === 'string' && developersService.developers.indexOf(name) === -1) {
                developersService.developers.push(name);
                return true;
            } 
            
            return false;
        },

        removeDeveloper: function (index) {
            if (typeof index === 'number') {
                developersService.developers.splice(index, 1);
                return true;
            }

            return false;
        },

        setDevelopersPairList: function (array) {
            if (Object.prototype.toString.call(array) === '[object Array]') {
                developersService.developersPairList = array;
                return true;
            }

            return false;
        },

        getDevelopersPairList: function () {
            return developersService.developersPairList;
        }
    }

    return developersService;
}]);